/*
 * spacemenu-show.cpp
 *
 * Copyright (c) 2014 Alexey Shurygin <batekman@gmail.com>
 *
 * This file is part of spacemenu.
 *
 * spacemenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * spacemenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with spacemenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "libspacemenu/spacemenu.hpp"

int recursivePrint(spacemenu *s, int tablength)
{
    std::vector<spacemenu*> lines;
    std::vector<spacemenu*>::iterator iterator;

    lines = s->elements();
    iterator = lines.begin();

    while(iterator != lines.end())
    {
        int i = 0;

        while(i<tablength)
        {
            std::cout << "\t";
            ++i;
        }

        std::cout << (*iterator)->title() << std::endl;

        if((*iterator)->isMenu())
            recursivePrint(*iterator, tablength+1);

        ++iterator;
    }

    return true;
}

int main(int argc, char* argv[])
{
    int tab = 0;

    spacemenu *s = new spacemenu("menu.txt");

    recursivePrint(s, tab);

    delete s;

    return 0;
}
