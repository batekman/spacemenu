spacemenu
=========

Menu generator with minimalistic source format.

### Planned features
* Native pipe support
* Qt4/Qt5 GUI
* Window mode (like in systemsettings)
* "Start menu" mode (like same in Unity)

### Menu example
<pre>
qupzilla
krusader
* games
    * action
        openarena " OpenArena
    * racing
        supertuxkart " SuperTuxKart
        trigger
-
%Exit
</pre>




### Requisites for donations

**WMR:** R210609141440

**WMZ:** Z134616197459

**WMU:** U383550974742