/*
 * libspacemenu/spacemenu.cpp
 *
 * Copyright (c) 2014 Alexey Shurygin <batekman@gmail.com>
 *
 * This file is part of spacemenu.
 *
 * spacemenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * spacemenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with spacemenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spacemenu.hpp"

// Constructor
/*************************************************************/

spacemenu::spacemenu()
{
    spacemenu::_initValues();
}

spacemenu::spacemenu(std::string menufile)
{
    spacemenu::_initValues();

    spacemenu::setMenufile(menufile);
    if(!spacemenu::loadFile()) { std::exit(1); }
    spacemenu::parseLines();
}

spacemenu::spacemenu(spacemenu *parent)
{
    spacemenu::_initValues();
    spacemenu::setIsSubMenu(true);

    spacemenu::setParent(parent);
}

spacemenu::spacemenu(spacemenu *parent, std::string title)
{
    spacemenu::_initValues();
    spacemenu::setIsSubMenu(true);

    spacemenu::setParent(parent);
    spacemenu::setTitle(title);
}


// Destructor
/*************************************************************/

spacemenu::~spacemenu()
{
    std::vector<spacemenu*> elements;
    std::vector<spacemenu*>::iterator iterator;

    if(spacemenu::isMenu())
    {
        elements = spacemenu::elements();
        iterator = elements.begin();

        while(iterator != elements.end())
        {
            delete (*iterator);
            ++iterator;
        }
    }

}

/*************************************************************/


// Initialize private values
/*************************************************************/
int spacemenu::_initValues()
{
    spacemenu::setIsMenu(true);
    spacemenu::setIsSubMenu(false);
    spacemenu::setIsPipeMenu(false);
    spacemenu::setIsExit(false);

    spacemenu::_lastChild = this;
    spacemenu::_lastSubMenu = this;

    spacemenu::_menufile = (std::string)getenv("HOME") + "/.config/spacemenu/menu.txt";

    spacemenu::_command = "";
    spacemenu::_title = "";

    return true;
}
/*************************************************************/


// Set parameters
/*************************************************************/

int spacemenu::setParent(spacemenu *parent)
{
    if(parent != NULL)
        spacemenu::_parent = parent;
    else
        return false;

    return true;
}

int spacemenu::setMenufile(std::string menufile)
{
    if(menufile != "")
    {
        spacemenu::_menufile = menufile;
    }

    return true;
}

/**************************************************************/


// Get parameters
/*************************************************************/

std::string spacemenu::menufile() const
{
    return spacemenu::_menufile;
}


// Element flags
/*************************************************************/

// Setters

int spacemenu::setIsMenu(bool flag)
{
    spacemenu::_isMenu = flag;

    if(!flag)
    {
        spacemenu::setIsSubMenu(false);
        spacemenu::setIsPipeMenu(false);
    }

    return true;
}

int spacemenu::setIsSubMenu(bool flag)
{
    spacemenu::_isSubMenu = flag;

    if(flag)
        spacemenu::setIsMenu(true);

    return true;
}

int spacemenu::setIsPipeMenu(bool flag)
{
    spacemenu::_isPipeMenu = flag;

    if(flag)
        spacemenu::setIsMenu(true);

    return true;
}

int spacemenu::setIsExit(bool flag)
{
    spacemenu::_isExit = flag;

    if(flag)
        spacemenu::setIsMenu(false);

    return true;
}

int spacemenu::setIsSeparator(bool flag)
{
    spacemenu::_isSeparator = flag;

    if(flag)
        spacemenu::setIsMenu(false);
        spacemenu::setTitle("-");

    return true;
}

// Getters

bool spacemenu::isMenu() const
{
    return spacemenu::_isMenu;
}

bool spacemenu::isSubMenu() const
{
    return spacemenu::_isSubMenu;
}

bool spacemenu::isPipeMenu() const
{
    return spacemenu::_isPipeMenu;
}

bool spacemenu::isExit() const
{
    return spacemenu::_isExit;
}

bool spacemenu::isSeparator() const
{
    return spacemenu::_isSeparator;
}

/*************************************************************/


// Private arrays getter
/*************************************************************/

std::vector<spacemenu*> spacemenu::elements()
{
    return spacemenu::_elements;
}

std::vector<std::string> spacemenu::fileLines()
{
    return spacemenu::_file_lines;
}

/*************************************************************/


// Put file lines into private array
/*************************************************************/

int spacemenu::loadFile()
{
    std::ifstream f(spacemenu::menufile().c_str());

    if(!f.is_open())
    {
        std::cerr << "ERROR when opening "
                  << spacemenu::menufile()
                  << std::endl;

        return false;
    }

    std::string line;

    while(getline(f, line))
    {
        spacemenu::_file_lines.push_back(line);
    }

    f.close();

    return true;
}

/*************************************************************/


// Parse lines from array
/*************************************************************/

int spacemenu::parseLines()
{
    std::vector<std::string> lines;
    std::vector<std::string>::iterator iterator;
    std::size_t finder;
    std::string line;

    bool sub = false;

    lines = spacemenu::fileLines();
    iterator = lines.begin();

    while(iterator != lines.end())
    {
        line = *iterator;

        if((*iterator).empty())
        {
            ++iterator;
            continue;
        }
        if (
                (spacemenu::isMenu())
                &&
                (line.compare(0, 4, "    ") == 0)
                &&
                (line.compare(4, 8, "    ") != 0)
            )
        {
            sub = true;
            spacemenu::lastSubMenu()->addLine(line.substr(4, line.length()));
        }
        else
        {
            if(
                (spacemenu::lastSubMenu() != this) &&
                (sub == true)
            )
                spacemenu::lastSubMenu()->parseLines();

            sub = false;

            spacemenu *element = new spacemenu(this);
            spacemenu::_elements.push_back(element);

            line = strip(line);

            if(line.compare(0, 1, SYM_MENU) == 0)
            {
                element->setIsSubMenu(true);
                element->setTitle((line).substr(2, (line).length()));
                spacemenu::_subMenus.push_back(element);
                spacemenu::_lastSubMenu = element;
            }
            else if(line == "-")
            {
                element->setIsSeparator(true);
            }
            else
            {

                if ((line).compare(0, 1, SYM_PIPE) == 0)
                {
                    element->setIsPipeMenu(true);
                    line = (line).substr(1, (line).length());
                }
                else if ((line).compare(0, 1, SYM_EXIT) == 0)
                {
                    element->setIsExit(true);
                    line = (line).substr(1, (line).length());
                }
                else
                {
                    element->setIsMenu(false);
                    line = line;
                }

                finder = line.find(std::string(" ") + std::string(SYM_TITLE) + std::string(" "));

                if(finder != std::string::npos)
                {
                    element->setTitle(line.substr(finder + 3, line.length()));
                    element->setCommand(line.substr(0, finder));
                }
                else
                {
                    element->setTitle(line);
                }

            }
        }

        ++iterator;
    }

    if(
        (spacemenu::lastSubMenu() != this) &&
        (sub == true)
    )
        spacemenu::lastSubMenu()->parseLines();

    return true;
}


/*************************************************************/


// Title
/*************************************************************/

int spacemenu::setTitle(std::string title)
{
    spacemenu::_title = strip(title);

    if(spacemenu::_command.empty())
        spacemenu::setCommand(strip(title));

    return true;
}

std::string spacemenu::title() const
{
    return spacemenu::_title;
}

/*************************************************************/


// Command
/*************************************************************/

int spacemenu::setCommand(std::string command)
{
    spacemenu::_command = strip(command);

    return true;
}


std::string spacemenu::command() const
{
    if(spacemenu::_command.empty())
        return spacemenu::_title;
    else
        return spacemenu::_command;
}

/*************************************************************/


spacemenu* spacemenu::parent() const
{
    return spacemenu::_parent;
}

spacemenu* spacemenu::lastChild() const
{
    return spacemenu::_lastChild;
}

spacemenu* spacemenu::lastSubMenu() const
{
    return spacemenu::_lastSubMenu;
}

std::vector<spacemenu*> spacemenu::subMenus()
{
    return spacemenu::_subMenus;
}

int spacemenu::addLine(std::string line)
{
    spacemenu::_file_lines.push_back(line);

    return true;
}


// libparsehelper strip function
/*************************************************************/

std::string strip(std::string original)
{
    std::string s;
    std::string spacers(" \n\r\t");
    size_t finder;

    s = original;

    finder = s.find_last_not_of(spacers);

    if(finder != std::string::npos)
        s = s.substr(0, finder+1);

    finder = s.find_first_not_of(spacers);

    if(finder != std::string::npos)
        s = s.substr(finder, s.length());

    return s;
}

/*************************************************************/
