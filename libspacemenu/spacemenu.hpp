/*
 * libspacemenu/spacemenu.hpp
 *
 * Copyright (c) 2014 Alexey Shurygin <batekman@gmail.com>
 *
 * This file is part of spacemenu.
 *
 * spacemenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * spacemenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with spacemenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPACEMENU_HPP
#define SPACEMENU_HPP

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <cstdlib>


#define SYM_TITLE   "\""
#define SYM_MENU    "*"
#define SYM_PIPE    "@"
#define SYM_SEP     "-"
#define SYM_EXIT    "%"


enum
{
    SM_MENU,
    SM_ELEMENT,
    SM_PIPEMENU,
    SM_EXIT
};

// This function must be moved to libparsehelper.

std::string strip(std::string original);

// Main class

class spacemenu
{

public:

    spacemenu();
    spacemenu(std::string menufile);
    spacemenu(spacemenu* parent);
    spacemenu(spacemenu* parent, std::string title);
    ~spacemenu();

    int setMenufile(std::string);
    std::string menufile() const;

    int setParent(spacemenu* parent);

    int setTitle(std::string title);
    std::string title() const;

    int setCommand(std::string command);
    std::string command() const;

    int loadFile();
    int parseLines();

    std::vector<std::string> fileLines();
    std::vector<spacemenu*> elements();
    std::vector<spacemenu*> subMenus();

    spacemenu* lastChild() const;
    spacemenu* parent() const;
    spacemenu* lastSubMenu() const;

    int addLine(std::string line);


    // Element flags

    bool isMenu() const;
    bool isSubMenu() const;
    bool isPipeMenu() const;
    bool isExit() const;
    bool isSeparator() const;

    int setIsMenu(bool flag);
    int setIsSubMenu(bool flag);
    int setIsPipeMenu(bool flag);
    int setIsExit(bool flag);
    int setIsSeparator(bool flag);

private:

    int _initValues();

    std::string _menufile;

    std::string _title;
    std::string _command;

    bool _isMenu;
    bool _isSubMenu;
    bool _isPipeMenu;
    bool _isExit;
    bool _isSeparator;

    std::vector<std::string> _file_lines;
    std::vector<std::string> _menu_lines;
    std::vector<spacemenu*> _elements;
    std::vector<spacemenu*> _subMenus;

    spacemenu *_parent;
    spacemenu *_lastChild;
    spacemenu *_lastSubMenu;

};

#endif // SPACEMENU_HPP
