cmake_minimum_required(VERSION 2.8)

project(libspacemenu)

set(CMAKE_SHARED_LIBRARY_PREFIX "")

add_library(libspacemenu SHARED spacemenu.cpp)

install(TARGETS libspacemenu DESTINATION lib)
