/*
 * spacemenu-openbox.cpp
 *
 * Copyright (c) 2014 Alexey Shurygin <batekman@gmail.com>
 *
 * This file is part of spacemenu.
 *
 * spacemenu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * spacemenu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with spacemenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstdlib>
#include "libspacemenu/spacemenu.hpp"

std::string tabulate(int count)
{
    std::string s("");
    int i = 0;
    while(i<count)
    {
        s = s + std::string("    ");
        ++i;
    }

    return s;
}

int recursivePrint(spacemenu *s, int level)
{
    std::vector<spacemenu*> lines;
    std::vector<spacemenu*>::iterator iterator;

    lines = s->elements();
    iterator = lines.begin();

    while(iterator != lines.end())
    {
        std::cout << tabulate(level);

        if((*iterator)->isMenu())
        {
            if((*iterator)->isPipeMenu())
            {
                std::cout
                        << std::endl
                        << "<menu label=\""
                        << (*iterator)->title()
                        << "\" execute=\""
                        << (*iterator)->command()
                        << "\" />"
                        << std::endl << std::endl;
            }
            else
            {
                std::cout
                        << std::endl
                        << tabulate(level)
                        <<"<menu label=\""
                        << (*iterator)->title()
                        << "\">"
                        //<< std::endl
                        << std::endl
                        << std::endl;

                recursivePrint(*iterator, level+1);

                std::cout
                        << tabulate(level)
                        << "</menu>"
                        << std::endl
                        << std::endl;
            }
        }
        else if((*iterator)->isExit())
        {
            std::cout
                    << "<item label=\""
                    << (*iterator)->title()
                    << "\">"
                    << std::endl
                    << tabulate(level+1)
                    << "<action name=\"Exit\"></action>"
                    << std::endl
                    << tabulate(level)
                    << "</item>"
                    << std::endl;
        }
        else if((*iterator)->isSeparator())
        {
            std::cout
                    << "<separator />"
                    << std::endl
                    << std::endl;
        }
        else
        {
            std::cout
                    << "<item label=\""
                    << (*iterator)->title()
                    << "\">"
                    << std::endl
                    << tabulate(level+1)
                    << "<action name=\"Execute\">"
                    << std::endl
                    << tabulate(level+2)
                    << "<command>"
                    << (*iterator)->command()
                    << "</command>"
                    << std::endl
                    << tabulate(level+1)
                    << "</action>"
                    << std::endl
                    << tabulate(level)
                    << "</item>"
                    << std::endl;
        }

        std::cout << std::endl;

        ++iterator;
    }

    return true;
}

int main(int argc, char* argv[])
{
    std::string fname;
    int level = 0;

    puts("<openbox_pipe_menu>\n");

    if(argc>=2)
        fname = argv[1];

    spacemenu *s = new spacemenu(fname);

    recursivePrint(s, level);

    delete s;

    puts("</openbox_pipe_menu>");

    return 0;
}
